/*
  Arduino program for Eve in the demonstration of the BB84 protocol. Based on the ArduinoQuantum of Qcumber (2018).
  
  Polarizers changes manually, without steppers!
  Authors of these modifications: K. Pawłowski, W. Wlodzynski
*/

//TODO PRZESTAN ZASILAC KLUCZ GDY PRZEKROCZY 16
// TODO possibleKey 


const int serialTimeout = 100; // 100 ms

// Important parameters
const int seqLength = 16;  // Polarisation sequence length (16 bit)


//const int sensorLoc1 = A1;   //  Wlasciwe podlaczenie
const int sensorLoc2 = A0;   // Pin for the second sensor
const int sensorLoc1 = A2;   // Tylko testy!

const int catchTh = 200;   // Threshold in CATCH command ~1V. //TODO ADJUST!

////////////////////////////////////////////////
// EEPROM configuration ////////////////////////
const int EEloc_angleCurrent = 0; // Takes 2 bytes
const int EEloc_angleTarget = 2;  // Takes 2 bytes
int polOffset = 0; // Polarisation offset, retrieve and store in EEProm
const int EEloc_polOffset = 4;  // Takes 2 bytes

////////////////////////////////////////////////
// Classical communication /////////////////////
const int seqClassicBlink = 100;
const int msgLength = 1; // DEBUG

char buff[100];
////////////////////////////////////////////////

// Eve sees, that signals belongs to 4 categories (0 or 1, but in two possible basis), called in this program 0, 1, 2, 3
const int nSeqType = 4;

// Eve doesn't know which signals means 0, and which 1
// There are 6 ways to choose 2 zeros and 2 ones for the four types  {0, 1, 2, 3}
// .. Comment: using correlations between signals (the weakest and the strongest are in the same basis) there are in fact 4 ways. 
const int nPossibleKeys = 6; 

// table to store possible keys (different mappings of 0, 1 to measures signals)
int possibleKeys[nPossibleKeys][seqLength] = {0};


// values of the refence voltages
int sensitivityDefault = 30;
int sensitivity2 = 0;
int refVoltage1[nSeqType] = {0}; // int datatype, reference voltages for the possible 4 signals, determined after the first sequence of 16 bits
int refVoltage2[nSeqType] = {0};

// auxilary table - last signals, classified into 4 categories
int lastSeq[seqLength]={};

//measured values of voltages for both sensors
int sensorArrVal1[seqLength] = {0};
int sensorArrVal2[seqLength] = {0};

// nie mam pojecia co to
int baseSeq[seqLength] = {0}; // int datatype, in multiples of 45 degrees.

// change to 1, when reference voltage for the four signals will be determined
int isReferenceValues = 0;

// current position of the key
int ikeyPosition = 0;

// Times - the same as for sender and receiver
const int seqStepTime = 1500;  // Time between each steps. Def: 1500 ms
const int seqInitTarget = 1;   // Set initialization polarisation for seq always (D)
const int seqPinStart = 1200;  // Time in sequence to start / ON the pin
const int seqPinStop = 1400;   // Time in sequence to start / ON the pin
const int seqReadTime = 1300;  // 1300 ms (hopefully in the middle of the laser pulse)
const int seqSyncBlink = 500;  // 200 ms (to initialise the signal)

int getMaxVoltage(){
  return max(analogRead(sensorLoc1), analogRead(sensorLoc2));
}


void setup() {
  Serial.begin(115200);
  Serial.setTimeout(serialTimeout);
  
  pinMode(sensorLoc1, INPUT);
  pinMode(sensorLoc2, INPUT);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// START LOOP ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void loop() {
  char serbuf[8] = ""; // Reinitialise buffer (8 bytes)
  Serial.readBytesUntil(' ', serbuf, 8); // Until whitespace
  int enumc = -1; // default choice
  int maxChoice = 10;
  char sercmd[maxChoice][9] = {"HELP", "VOLT1?", "VOLT2?", "CATCH",   // 4
  "EVESEQ", "BASIS", "CLASSIF", "RESETK", "KEY?", "RMSG"};            //10
  
  if(serbuf[0]!='\0'){ 
    for (int c=0; c<maxChoice; c++){
        if (strcasecmp(sercmd[c],serbuf) == 0){
            enumc = c;// Obtain match
        }
    }
   
    char valbuf[8] = "";      // Buffer to receive chartype value from serial
    char baseseqbuf[seqLength] = ""; // Buffer to receive chartype pol sequences from serial     
    
    switch(enumc){  
      case 0: //HELP
        Serial.print(F("Quantum Key Construction (Mission 2)\n"));
        Serial.print(F("HELP       Print help statement\n"));
        Serial.print(F("VOLT1?     Ask for sensor1 voltage\n"));
        Serial.print(F("VOLT2?     Ask for sensor2 voltage\n"));
        Serial.print(F("CATCH      Wait for laser light and display time\n"));
        Serial.print(F("EVESEQ     Catch the whole sequence\n"));
        Serial.print(F("BASIS X    Typing basis (1 - common for Alice and Bob, 0 - not matching) and update key accordingly\n"));
        Serial.print(F("CLASSIF S  Signal post-processing, using sensitivity S\n")); //TODO BETTER NAME?
        Serial.print(F("RESETK     Reseting key\n"));
        Serial.print(F("KEY?       Printing key\n"));
        Serial.print(F("RMSG       Receive classical message\n"));
        Serial.print("\n"); break;    
        
      /////////////////////////////////////////////////////////////////////////////// 
      case 1: //VOLT1?
        Serial.print(F(" Voltage on the first sensor (in scale 0-1023) = "));
        Serial.println(analogRead(sensorLoc1));
        Serial.print("\n"); break;
        
      ///////////////////////////////////////////////////////////////////////////////
      case 2: //VOLT2?
        Serial.print(F(" Voltage on the second sensor (in scale 0-1023) = "));
        Serial.println(analogRead(sensorLoc2));
        Serial.print("\n"); break;

      ///////////////////////////////////////////////////////////////////////////////
      case 3: //CATCH
        Serial.print(lasCatch());
        Serial.println(" ms is when the light triggers.");
        Serial.print("\n"); break;
        
      ///////////////////////////////////////////////////////////////////////////////
      case 4: //EVESEQ
        Serial.println("Waiting for signal...");
        runSequenceEve();  
        Serial.println("OK");
        Serial.print("\n"); break;

      ///////////////////////////////////////////////////////////////////////////////
      case 5: //BASIS X    
          while (!Serial.available());
          Serial.readBytesUntil(' ', baseseqbuf, seqLength); // Until seqLength  
          transCharSeq(baseseqbuf, baseSeq, seqLength);
  
          Serial.println(F("You typed:"));
          for (int iB=0; iB < seqLength; ++iB){
                Serial.print(baseSeq[iB]);
          }
          Serial.print("\n");
  
  
          Serial.println(F("\nClassified signals"));  //TODO USUN WYPISYWANIE OSTATNICH SYGNALOW PO TESTACH
          for (int iSignal = 0; iSignal < seqLength; iSignal ++){
                  Serial.print(lastSeq[iSignal]);
          }
          Serial.println();
  
          for (int iB=0; iB < seqLength; ++ iB){
            
            if (baseSeq[iB] == 1){
              possibleKeys[0][ikeyPosition] = changeBit(lastSeq[iB], 0, 0, 1, 1);
              possibleKeys[1][ikeyPosition] = changeBit(lastSeq[iB], 0, 1, 0, 1);
              possibleKeys[2][ikeyPosition] = changeBit(lastSeq[iB], 0, 1, 1, 0);
              possibleKeys[3][ikeyPosition] = changeBit(lastSeq[iB], 1, 0, 0, 1);
              possibleKeys[4][ikeyPosition] = changeBit(lastSeq[iB], 1, 0, 1, 0);
              possibleKeys[5][ikeyPosition] = changeBit(lastSeq[iB], 1, 1, 0, 0);
              
              ikeyPosition++;
              if(ikeyPosition == seqLength) break;
            }
          }       
  
        Serial.println("KEY UPDATED GIVEN INFORMATION ABOUT BASIS");
        Serial.print("\n"); break;      

      ///////////////////////////////////////////////////////////////////////////////
      case 6: //CLASSIFY
        while (!Serial.available());
        Serial.readBytesUntil(' ', valbuf, 8); // Until whitespace
        sensitivity2 = atoi(valbuf);
        Serial.print("Sensitivity: ");
        Serial.println(sensitivity2);
          
        computeRefValues(sensorArrVal1, sensorArrVal2);
        classifySignals(sensitivity2);
        Serial.print(F("Reference intensities: "));
        Serial.println(isReferenceValues);
        
        Serial.println(F("\nCLASSIFIED\n"));
        Serial.print("\n"); break;
        
      ///////////////////////////////////////////////////////////////////////////////
      case 7: //RESETK
        //TODO: Czy wszystko wyzerowane?
        isReferenceValues = 0;
        for(int i=0; i < nSeqType; i++ ){
          refVoltage1[i] = 0;
          refVoltage2[i] = 0; 
        }
        
        ikeyPosition = 0;
        
        for(int i=0; i<nPossibleKeys; i++){
          for(int j=0; j<seqLength; j++){
            possibleKeys[i][j] = 0;
          }
        }
        
        Serial.println("RESETING KEY");
        Serial.print("\n"); break;
        
      ///////////////////////////////////////////////////////////////////////////////
      case 8: //KEY?
        Serial.print(F("Currently key consists of "));
        Serial.print(ikeyPosition);
        Serial.println(" bits:");
        
        for (int nrow=0; nrow < nPossibleKeys;  nrow++){
          Serial.print(F("Version "));
          Serial.print(nrow);
          Serial.print(F(" "));
          for(int i=0; i < ikeyPosition; i++){
            Serial.print(possibleKeys[nrow][i]); 
          }
        Serial.print("\n");
        }
        Serial.print("\n"); break;
        
        ///////////////////////////////////////////////////////////////////////////////
        case 9: //RMSG
        {
            Serial.println(F("Waiting for signal..."));
    
            int msg_len = 0;
            unsigned long timeStep;
            
            while (getMaxVoltage() < catchTh);
            timeStep = millis()+seqClassicBlink+seqClassicBlink/2;
    
            for(int i=0; i<6; i++){
                while(millis()<timeStep);
                msg_len += (getMaxVoltage()> catchTh)*(1<<i); 
                timeStep += seqClassicBlink;
            }
            uint8_t msg_bin[msg_len*5];
            Serial.print("Message length: ");
            Serial.println(msg_len);
  
            for(int i=0; i < 5*msg_len; i++) {
                while(millis()<timeStep);
                msg_bin[i] = 1*(getMaxVoltage()> catchTh);
                timeStep += seqClassicBlink;
            }
            
            Serial.println(F("Binary coded message:"));
            for(int i=0; i<5*msg_len; i++){ if(i%5==0 && i!=0){Serial.print("-");} Serial.print(msg_bin[i]);  }
            Serial.print("\n");
            
            uint8_t msgBinDecoded[nPossibleKeys][msg_len*5];
  
            ////////////////////////////////////////////////
            Serial.println(F("Binary decoded message:"));
            for (int nrow=0; nrow < nPossibleKeys;  nrow++){
                Serial.print(F("Version "));
                Serial.print(nrow);
                Serial.print(F(": "));
                
                for(int i=0; i<msg_len*5; i++){
                    if(i%5==0 && i!=0){Serial.print("-");}
                    msgBinDecoded[nrow][i] = (msg_bin[i]+uint8_t(possibleKeys[nrow][i%ikeyPosition]))%2;
                    Serial.print(msgBinDecoded[nrow][i]);
                    
                }
                Serial.print(" -> ");
                for(int i=0; i<msg_len; i++){
                      int ltr = 0;
                      for(int j=4; j>=0; j--){
                        if( msgBinDecoded[nrow][5*i+j] ) ltr += (1<<(4-j));
                      }
                      Serial.print(char(ltr+97));
                  }
                Serial.print("\n");
            }
          Serial.print("\n"); break;
        }
        ////////////////////////////////////////////////////
        default:
          Serial.println("Unknown command");
          Serial.print("\n"); break;
    }
  }

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// END LOOP /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

unsigned long lasCatch(){
  int sensorValue1 = 0;
  int sensorValue2 = 0;
  int sensorValueTotal = 0;
  
  while (sensorValueTotal < 1.5*catchTh){
    sensorValue1 = analogRead(sensorLoc1);
    sensorValue2 = analogRead(sensorLoc2);
    Serial.println(sensorValue1+sensorValue2);
    sensorValueTotal = sensorValue1 + sensorValue2;
  }
  return millis();
}

void transCharSeq(char baseseqbuf[], int baseSeq[], int seqLength){
  // Translate char array to int array
  int basenow;
 
  // Iterating over the array
  for(int i=0; i<seqLength; i++){
    basenow = (int)baseseqbuf[i] - 48;  // Convert ASCII char to int
    if(basenow == -48) basenow = 0;  // Convert all nulls to zero ints (if sequences less than 64 digits)
    if(basenow < 0 || basenow > 1){  // Convert all nonsense values to 0
      Serial.println("Input error detected.");
      basenow = 0; // Skipping that particular input
    }
     
    baseSeq[i] = basenow; // Update the int array 
  }
}

void runSequenceEve(){
  //  Function for Eve -  finally it should be incorporated in the program common for Bob and Alice, to have exactly the same time parameters
  // Otherwise, one should save measured signals to post-process them
  
  unsigned long timeStep = 0;

  
  for(int i=0; i<seqLength; ++i){  //Clear auxilary global tables before the next sequence
    sensorArrVal1[i] = 0;
    sensorArrVal2[i] = 0;
  }
  
  timeStep = lasCatch()+2*seqSyncBlink;    //Caught the sync signal
  
  //delay(2*seqSyncBlink);
  while(analogRead(sensorLoc1)+analogRead(sensorLoc2) > 1.5*catchTh);
  delay(1500);
  
  // Main sequence
  for(int i=0; i<seqLength; i++){
    while(analogRead(sensorLoc1)+analogRead(sensorLoc2) < 1.5*catchTh);
    //while(millis()<timeStep);
   // delay(seqReadTime);

    
    sensorArrVal1[i] = analogRead(sensorLoc1);
    sensorArrVal2[i] = analogRead(sensorLoc2);
    sprintf(buff,"%d/%d ",i+1,seqLength);
    Serial.print(buff);

    while(analogRead(sensorLoc1)+analogRead(sensorLoc2) > 1.5*catchTh);
    delay(200);
   // delay(seqStepTime - seqReadTime);
    
    timeStep += seqStepTime;  // Update the timeStep
  }
  Serial.println();
   
  // Sends the measurement values to serial 
   for (int i=0; i<seqLength; i++){
      Serial.print(sensorArrVal1[i]);
      Serial.print(" ");
      Serial.println(sensorArrVal2[i]);
    }
}


void computeRefValues(int sensorArrVal1[], int sensorArrVal2[]){ 
/*
  Serial.println("Data from sensors:");
  for (int i; i< seqLength; ++i){
    Serial.print(sensorArrVal1[i]);
    Serial.print(" " );
    Serial.println(sensorArrVal2[i]);
  }
  Serial.print("\n");
*/

  int decodedArray[seqLength] = {0};
  for(int iSignal=0; iSignal<seqLength; iSignal++){
    decodedArray[iSignal] = -1;
  }

  
  for(int iRef = 0 ; iRef < nSeqType; iRef++){
    
    //finding first -1
    int iX = findX(decodedArray);
    //Serial.print(iX);

    int volRefCand1 = sensorArrVal1[iX];
    int volRefCand2 = sensorArrVal2[iX];
    
    int nrSignals = 0;
    float volRef1 = 0;
    float volRef2 = 0;
      

    float diff1 = 0.0;
    float diff2 = 0.0;
    for(int iSignal=iX; iSignal<seqLength; iSignal++){
        
      diff1 = sensorArrVal1[iSignal] - volRefCand1;
      diff2 = sensorArrVal2[iSignal] - volRefCand2;
        
      if( sqrt(diff1*diff1 + diff2*diff2) < sensitivityDefault){       
        decodedArray[iSignal] = iRef;
        volRef1 += sensorArrVal1[iSignal];
        volRef2 += sensorArrVal2[iSignal];
        nrSignals ++;
      }
    }

    if(nrSignals > 0){
        volRef1 = volRef1 * 1. / nrSignals;
        volRef2 = volRef2 * 1. / nrSignals;
    }

    refVoltage1[iRef] = volRef1;
    refVoltage2[iRef] = volRef2;
    
  }
  Serial.print(F("\n\nReference Values:\n"));
  for(int i=0; i<nSeqType; ++i){
    sprintf(buff,"%d %d\n",refVoltage1[i],refVoltage2[i]);
    Serial.print(buff);
  }

  isReferenceValues = 1;
  Serial.println(F("Reference values computed"));
}

void classifySignals(int sensitivity){
  Serial.print(F("SIGNAL CLASSIFICATION"));

  // Clear decoded array to fill it with signals 0, 1, 2, 3 afterwards
  for(int iSignal=0; iSignal<seqLength; iSignal++){
    lastSeq[iSignal] = -1;
  }

  for (int iSignal = 0; iSignal < seqLength; iSignal ++){
    int nearestRefPoint = 0;
    uint32_t distX = sensorArrVal1[iSignal]-refVoltage1[0];
    uint32_t distY = sensorArrVal2[iSignal]-refVoltage2[0];
    uint32_t squaredDistanceToNearestRefPoint = distX*distX+distY*distY;
    for(int iRef=1; iRef<nSeqType; iRef++){
      distX = sensorArrVal1[iSignal]-refVoltage1[iRef];
      distY = sensorArrVal2[iSignal]-refVoltage2[iRef];
      uint32_t squaredDistanceToRefPoint = distX*distX+distY*distY;
      if(squaredDistanceToRefPoint < squaredDistanceToNearestRefPoint){
        squaredDistanceToNearestRefPoint = squaredDistanceToRefPoint;
        nearestRefPoint = iRef;
      }
    }
    
    if(squaredDistanceToNearestRefPoint <= sensitivity*sensitivity){
        lastSeq[iSignal] = nearestRefPoint;
    }
  }
      
  Serial.println(F("\nClassified signals"));
  for (int iSignal = 0; iSignal < seqLength; iSignal ++){
    if(lastSeq[iSignal]==-1){
      Serial.print("?");
    } else {
      Serial.print(lastSeq[iSignal]);
    }
  }
  Serial.println();
}

int findX(int decodedArray[]){
  for (int i=0; i<=seqLength; i++){
    if ( decodedArray[i] == -1){
      return i;
    }
  }
  return -123;
}


int changeBit(int inputSignal, int b0, int b1, int b2, int b3){
  int outputSignal;
  
  if( inputSignal == 0){
    outputSignal = b0;
  }else if( inputSignal == 1){
    outputSignal = b1;
  }else if( inputSignal == 2){
    outputSignal = b2;
  }else if( inputSignal == 3){
    outputSignal = b3;
  }else{
    outputSignal = -2;
  }

  sprintf(buff,"Input= %d    Mask= %d%d%d%d    Result= %d\n",inputSignal, b0,b1,b2,b3, outputSignal);
  Serial.print(buff);
  
  return outputSignal;
}
